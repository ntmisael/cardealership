# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.0.0]

### Added

- Continuous integration pipeline with Jenkins
- Profiles in the pom.xml
- PostgreSql configuration

